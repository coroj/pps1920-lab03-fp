package lab03

import lab03.Lab03._
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Optionals.Option._
import u02.SumTypes.{Student, Teacher}
import u03.Lists.List.{Cons, Nil}
import u03.Streams._



class Lab03Test {

  val lst: Cons[Int] = Cons(10, Cons(20, Cons(30, Nil())))
  val foldLst: Cons[Int] = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))
  @Test def test1A(){
    assertEquals(Nil(),drop(Nil(),5))
    assertEquals(Cons(20,Cons(30, Nil())),drop(lst ,1))
    assertEquals(Cons(30, Nil()),drop(lst ,2))
    assertEquals(Nil(),drop(lst ,5))
  }
  @Test def test1B(): Unit ={
    assertEquals(Cons(11,Cons(21,Cons(31,Nil()))),Lab03.flatMap(lst)(v => Cons(v+1, Nil())))
    assertEquals(Nil[Int](),Lab03.flatMap(Nil[Int]())(_ => Cons(1, Nil())))
    assertEquals(Cons(11,Cons(12,Cons(21,Cons(22,Cons(31,Cons(32,Nil()))))))
    ,Lab03.flatMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))))
  }
  @Test def test1C(): Unit ={
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), Lab03.map(lst)(_ + 1))
    assertEquals(Cons(Cons(11, Cons(12, Nil())),
                      Cons(Cons(21, Cons(22, Nil())),
                       Cons(Cons(31, Cons(32, Nil())), Nil()))),Lab03.map(lst)(v => Cons(v+1,Cons(v+2,Nil()))))
  }
  @Test def test1D(): Unit ={
    assertEquals(Cons(20, Cons(30, Nil())), Lab03.filter(lst)(_ > 10))
  }

  @Test def test2(): Unit ={
    assertEquals(Some(25),max(Cons(10,Cons(25,Cons(20,Nil())))))
    assertEquals(None(),max(Nil()))
  }
  @Test def test3(): Unit ={
    assertEquals(Nil(),coursesByTeachers(Nil()))
    assertEquals(Nil(),coursesByTeachers(Cons(Student("Studente",2000),Nil())))
    assertEquals(Cons("Subject1",Nil()),coursesByTeachers(Cons(Teacher("Teacher1","Subject1"),
      Cons(Student("Student1",2000),Nil()))))
  }
  @Test def test4(): Unit ={

    assertEquals(-16,foldLeft(foldLst)(0)(_-_))
    assertEquals(16,foldLeft(foldLst)(0)(_+_))
    assertEquals(105,foldLeft(foldLst)(1)(_*_))
    assertEquals(0,foldLeft(Nil())(0)(_-_))

    assertEquals(-8,foldRight(foldLst)(0)(_-_))
    assertEquals(16,foldRight(foldLst)(0)(_+_))
    assertEquals(105,foldRight(foldLst)(1)(_*_))
    assertEquals(0,foldRight(Nil())(0)(_-_))
  }
  @Test def test5(): Unit ={
    val s = Stream.iterate (0)(_+1)
    assertEquals(Nil(),Stream.toList(Stream.drop(Stream.empty[Int]())(5)))
    assertEquals(Cons(6,Cons(7,Cons(8,Cons(9,Nil())))),Stream.toList(Stream.take(Stream.drop(s)(6))(4)))
    assertEquals(Cons(6,Cons(7,Cons(8,Cons(9,Nil())))),Stream.toList(Stream.drop(Stream.take(s)(10))(6)))
    assertEquals(Nil(),Stream.toList(Stream.drop(Stream.take(s)(10))(11)))
  }
  @Test def test6(): Unit ={
    val s = Stream.take(Stream.constant("x"))(5)
    assertEquals(Cons("x",Cons("x",Cons("x",Cons("x",Cons("x",Nil()))))),Stream.toList(s))
  }
  @Test def test7(): Unit ={
    val s = Stream.take(Stream.fibs)(8)
    assertEquals(Cons(0,Cons(1,Cons(1,Cons(2,Cons(3,Cons(5,Cons(8, Cons(13,Nil())))))))),Stream.toList(s))
  }
}