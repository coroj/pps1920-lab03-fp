package lab03


import u03.Lists.List.{Cons, Nil}
import u03.Lists._
import u02.Optionals._
import u02.Optionals.Option._
import u02.SumTypes.{Person, Teacher}

object Lab03{

  //1A
  @scala.annotation.tailrec
  def drop[A](l:List[A], n:Int):List[A] = l match{
    case Cons(_,t) if n>0 =>drop(t,n-1)
    case _ => l
  }
  //1B
  def flatMap[A,B](l:List[A])(f: A=>List[B]):List[B] = l match{
    case Cons(h,t) => List.append(f(h),flatMap(t)(f))
    case _ => Nil()
  }
  //1C
  def map[A,B](l:List[A])(f: A=>B):List[B] = flatMap(l)(v => Cons(f(v),Nil()))
  //1D
  def filter[A](l:List[A])(predicate:A=>Boolean):List[A] = flatMap(l){
    case v if predicate(v) => Cons(v,Nil())
    case _ => Nil()
  }
  //2
  def max(l: List[Int]):Option[Int] = l match{
    case Cons(h,t) =>max(t) match{
      case Some(v) if v>h => Some(v)
      case _ => Some(h)
    }
    case _ => None()
  }
  //3
  def coursesByTeachers(l:List[Person]):List[String] = flatMap(l){
    case Teacher(_, course) => Cons(course,Nil())
    case _ => Nil()
  }

  //4
  private def reduce():Int =  0

  def foldLeft(l:List[Int])(acc:Int)(op: (Int,Int) => Int):Int = {
    @scala.annotation.tailrec
    def _foldLeft(l:List[Int], acc:Int, op: (Int,Int) => Int) : Int =
      l match{
        case Cons(h,t) => _foldLeft(t, op(acc,h),op)
        case _ => acc
      }

    l match{
      case Nil() => reduce()
      case _ =>  _foldLeft(l,acc,op)
    }
  }

  def foldRight(l:List[Int])(acc:Int)(op: (Int,Int) => Int):Int = {
    def _foldRight(l:List[Int], acc:Int, op: (Int,Int) => Int) : Int = l match{
      case Cons(h,t) => op(h,_foldRight(t,acc,op))
      case _ => acc
    }

    l match{
      case Nil() => reduce()
      case _ => _foldRight(l,acc,op)
    }

  }

  //5,6,7 in Streams.scala

}
